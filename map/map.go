package main

import (
	"fmt"
)

func main() {
	m := make(map[string]int)
	m["a"] = 1
	m["b"] = 2
	m["c"] = 3
	fmt.Println(m)
	fmt.Println(m["a"])

	m2 := map[string]int{
		"a": 1,
		"b": 2,
		"c": 3,
	}
	fmt.Println(m2)

}
