package main

func main() {
	var ch = make(chan int)

	go func() {
		ch <- 1
	}()
	v := <-ch
	println(v)
}