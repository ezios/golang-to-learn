package main
import "fmt"

type Vehicle struct {
	Name string
	Descp string
	Year int
}

func (this *Vehicle) outputYear() {
	fmt.Printf("%s is producted in %v",this.Name,this.Year)
}

//使用万能接口类接收
func useUniversal(arg interface{})  {
	if arg != nil {
		// 类型断言
		vehicle := arg.(Vehicle)
		vehicle.outputYear()
	}
}

func main() {
	vehicle := Vehicle{Name:"Bugatti Veyron",Descp:"Super Sport Car",Year:2012}
	useUniversal(vehicle)
}