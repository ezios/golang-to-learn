package main

import (
	"fmt"
)

// 接口
type BirdInterface interface {
	fly()
	eat()
}
// 抽象类
type AbstractBird struct {
	Name string
}
// 具体类
type BuguBird struct {
	AbstractBird
	Descp string
}

// 方法(实现接口)
func (this AbstractBird) fly() {
	fmt.Printf("%s fly \n", this.Name)
}
// 方法(实现接口)
func (this AbstractBird) eat() {
	fmt.Printf("%s eat \n", this.Name)
}

func main() {
	var bird BirdInterface = BuguBird{
		AbstractBird{Name: "Bugu"} ,
		"BuguBird",
	}
	bird.fly()

	fmt.Println(bird)
}
