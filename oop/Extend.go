package main

import "fmt"

//父类
type Human struct {
	Username string
	Age int
	Sex string
}

func (_this *Human)walk()  {
	fmt.Printf("%s walk \n",_this.Username)
}

//子类继承父类
type SuperMan struct {
	Human

	Ability []string
}


func main() {
	man := Human{
		Username: "王五",
		Age: 30,
		Sex: "male",
	}
	fmt.Println(man)
	man.walk()

	PigMan := SuperMan{
		Human: Human{
			Username: "PicMan",
			Age: 20,
			Sex: "male",
		},
		Ability: []string{"fly", "swim"},
	}

	fmt.Println(PigMan.Username)
	fmt.Println(PigMan)
	PigMan.walk()
}