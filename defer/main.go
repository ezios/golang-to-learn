package main

import "fmt"

/**
* defer 延迟执行
*
*	defer存储方式为栈，执行顺序为后进先出
*
	defer是函数执行完之后，再执行。位于生命周期末端。
 */
func main() {

	defer fmt.Println("main defer 01")
	defer fmt.Println("main defer 02")
	defer fmt.Println("main defer 03")

	method()
}

func method() int{
	defer fmt.Println("method defer 01")
	defer fmt.Println("method defer 02")
	defer fmt.Println("method defer 03")
	fmt.Println("m")
	return retfunc()
}


func retfunc() int{
	fmt.Println("return执行")
	return 0
}
