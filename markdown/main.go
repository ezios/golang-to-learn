package main

import (
	"fmt"
	"os"
	"strings"
)

func main() {
	entrys , err := os.ReadDir("./")
	if err != nil {
		panic(err)
	}
	for _ , entry := range entrys {
		name := entry.Name()
		nameSlice := strings.Split(name, ".")
		if(len(nameSlice) > 1 && nameSlice[1] == "md"){
			fmt.Printf("[%s](./markdown/%s.md) \n",nameSlice[0],nameSlice[0])
		}
	}
}