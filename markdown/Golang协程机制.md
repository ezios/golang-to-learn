# 协程机制

```golang
package main

import (
	"fmt"
	"time"
)

func main() {

	fmt.Println("main run")
	go func() {
		for i := 0; i < 10; i++ {
			println("go routine 1", i)
			time.Sleep(1 * time.Second)
		}
	}()
	time.Sleep(5 * time.Second)

	fmt.Println("main end")

}

```