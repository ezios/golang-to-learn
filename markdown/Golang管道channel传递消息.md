# 管道channel传递消息

1. 无缓存的channel
```golang
package main

import (
	"fmt"
	"time"
)

/**
	无缓存的channel
	阻塞的一个取走，再发下一个
 * @Description: 协程
 */
func main() {
	var ch = make(chan int)

	go func() {
		for i := 0; i < 10; i++ {
			ch <- i
			fmt.Println("send " , i)
		}
	}()
	for {
		v := <-ch
		fmt.Println(v)
		time.Sleep(1 * time.Second)
	}
}
```

2. 有缓存的channel

```golang
package main

import (
	"fmt"
	"time"
)

/**
 *	有缓存的channel
 *	直到channel满，才阻塞的一个取走，再发下一个
 * @Description: 协程
 */
func main() {
	var ch = make(chan int,5)

	go func() {
		for i := 0; i < 10; i++ {
			ch <- i
			fmt.Println("send " , i)
		}
	}()
	for {
		v := <-ch
		fmt.Println(v)
		time.Sleep(1 * time.Second)
	}
}
```