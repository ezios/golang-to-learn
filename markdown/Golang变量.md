# 变量声明
```go
var a int
var a = 10
var a int = 10
var b, c int = 10, 20
a := 10 // 声明并赋值 这种不支持声明全局变量 
```

# 常量
```go
const globalConst = 10

//枚举
const (
    A = 1
    B = 2
    C = 3
)

// iota 枚举 按行递增 1 
const (
    A = iota * 10
    B
    C
)
```