# 第一个项目
运行`go`初始化项目
```
go mod init firstmod
go mod tidy
```
创建 `main.go`
```go
package main

import "fmt"

func main() {
    fmt.Println("Hello, World!")

}
```
运行
```
go run main.go
```
编译
```
go build main.go

# 得到二进制文件
./main
```