# 函数多返回值

```golang
import "fmt"

// 声明多个返回值
func twoRet(input1 int, input2 int) (int, int) {
	return input1 + input2, input1 * input2
}

func twoVarRet(input1 int,input2 int) (ret1 int,ret2 int)  {
	ret1 = input1 + input2
	ret2 = input1 * input2

	return
	
}
func twoVarRet2(input1 int,input2 int) (ret1,ret2 int)  {
	ret1 = input1 + input2
	ret2 = input1 * input2

	return
	
}
func main() {
	ret1, ret2 := twoRet(1, 2)
	fmt.Println(ret1, ret2)
	ret1, ret2 = twoVarRet(1, 2)
	fmt.Println(ret1, ret2)
	ret1, ret2 = twoVarRet2(1, 2)
	fmt.Println(ret1, ret2)
}
```

# 函数与包的情况

定义 `root/jLib1/jLib1.go` :
```golang
package jLib1

import "fmt"

// 初始化函数，引入后调用
func init() {
	fmt.Println("jLib1 init")
}
// 大写开头，暴露API
func Hello() {
	fmt.Println("Hello jLib1")
}
// 小写开头，内部使用
func bye() {
	fmt.Println("Bye jLib1")
}
```

`root/main.go`:
```golang
package main

import (
	"fmt"
	"learning/jLib1"
)

func main() {
    fmt.Println("Hello, World!")

	jLib1.Hello()
}
```

执行`main`函数
```golang
$ go run .
jLib1 init
Hello, World!
Hello jLib1
```

# lib包 别名

1. 给包起别名，可以通过别名调用包内部的函数。
2. 如果使用`.`引入包，则会将包内的方法导入到本包，直接调用，不需要加包名。
3. 如果包未使用，避免引入。


```golang
package main

import (
	"fmt"
	. "learning/jLib1"
	lib2 "learning/jLib2"
)

func main() {
    fmt.Println("Hello, World!")

	JLib1Hello()
	lib2.Hello()
}
```