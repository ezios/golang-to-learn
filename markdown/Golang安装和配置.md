# GoLang Installation
1. 官网下载最新版本的包: https://golang.org/dl/
2. 跟其他语言一样，解压到指定目录下.
3. 添加环境变量：

```
# 根目录
GOROOT=C:\xxx\Go 
# 工作目录 （一些运行脚本和库）
GOPATH=C:\Users\Administrator\xxx
# 可执行路径
PATH=%GOROOT%\bin;%GOPATH%\bin;
```

4. 验证安装是否成功：
```cmd
go version
```
5. 配置国内代理
```cmd
go env -w GO111MODULE=on
go env -w GOPROXY=https://goproxy.cn,direct

# 查看是否修改成功
go env

```
