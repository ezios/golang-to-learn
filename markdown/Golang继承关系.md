# 继承关系

以下代码中，`Human`结构体为“父类”，`SuperMan`结构体为“子类”。
1. 继承关系则在结构体声明下加上需要继承的`Human`结构体。
```golang
//子类继承父类
type SuperMan struct {
	Human // 继承

	Ability []string
}
```
2. 创建子类时，需要在创建语句中标明父类的结构体信息。

```golang
	PigMan := SuperMan{
		Human: Human{ // 标明继承的数据初始化
			Username: "PicMan",
			Age: 20,
			Sex: "male",
		},
		Ability: []string{"fly", "swim"},
	}
```

源码：

```golang
package main

import "fmt"

//父类
type Human struct {
	Username string
	Age int
	Sex string
}

func (_this *Human)walk()  {
	fmt.Printf("%s walk \n",_this.Username)
}

//子类继承父类
type SuperMan struct {
	Human

	Ability []string
}


func main() {
	man := Human{
		Username: "王五",
		Age: 30,
		Sex: "male",
	}
	fmt.Println(man)
	man.walk()

	PigMan := SuperMan{
		Human: Human{
			Username: "PicMan",
			Age: 20,
			Sex: "male",
		},
		Ability: []string{"fly", "swim"},
	}

	fmt.Println(PigMan.Username)
	fmt.Println(PigMan)
	PigMan.walk()
}
```