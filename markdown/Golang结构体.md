# 结构体
结构体是Go语言中一个重要的概念，它允许我们定义自己的类型，并使用这些类型来创建变量。结构体是一种数据类型，它允许我们定义一个变量，该变量可以包含多个不同的数据类型。

结构体定义如下：
```golang
package model

type Person struct {
	name string
	age  int
	sex  string
}

func SetPerson(this *Person,name string,age int,sex string)  {
	this.name = name
	this.age = age
	this.sex = sex
}

func (p *Person) SetName(name string)  {
	p.name = name
}
func (p *Person) SetAge(age int) {
	p.age = age
}
func (p *Person) SetSex(sex string)  {
	p.sex = sex
}
func (p *Person) GetName() string  {
	return p.name
}
func (p *Person) GetAge() int  {
	return p.age
}
func (p *Person) GetSex() string  {
	return p.sex
}

func Create() Person {
	return Person{}
}
```

以上这种方式参考了`OOP`封装的概念。这里小写的参数名代表外部无法访问，如果需要访问，无论方法还是变量名都应该大写开头。

`Golang`方法的写法是再func 后面的receiver里加一个接收变量的框，表示接收者，接收者可以是指针也可以是值。
