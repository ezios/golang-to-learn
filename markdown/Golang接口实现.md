# 接口实现

```golang
package main

import (
	"fmt"
)

// 接口
type BirdInterface interface {
	fly()
	eat()
}
// 抽象类
type AbstractBird struct {
	Name string
}
// 具体类
type BuguBird struct {
	AbstractBird
	Descp string
}

// 方法(实现接口)
func (this AbstractBird) fly() {
	fmt.Printf("%s fly \n", this.Name)
}
// 方法(实现接口)
func (this AbstractBird) eat() {
	fmt.Printf("%s eat \n", this.Name)
}

func main() {
	var bird BirdInterface = BuguBird{
		AbstractBird{Name: "Bugu"} ,
		"BuguBird",
	}
	bird.fly()

	fmt.Println(bird)
}

```


# 万能接口

```golang
package main
import "fmt"

type Vehicle struct {
	Name string
	Descp string
	Year int
}

func (this *Vehicle) outputYear() {
	fmt.Printf("%s is producted in %v",this.Name,this.Year)
}

//使用万能接口类接收
func useUniversal(arg interface{})  {
	if arg != nil {
		// 类型断言
		vehicle := arg.(Vehicle)
		vehicle.outputYear()
	}
}

func main() {
	vehicle := Vehicle{Name:"Bugatti Veyron",Descp:"Super Sport Car",Year:2012}
	useUniversal(vehicle)
}
```