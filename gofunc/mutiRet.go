package main

import "fmt"

// 声明多个返回值
func twoRet(input1 int, input2 int) (int, int) {
	return input1 + input2, input1 * input2
}
func twoVarRet(input1 int,input2 int) (ret1 int,ret2 int)  {
	ret1 = input1 + input2
	ret2 = input1 * input2

	return
	
}
func twoVarRet2(input1 int,input2 int) (ret1,ret2 int)  {
	ret1 = input1 + input2
	ret2 = input1 * input2

	return
	
}
func main() {
	ret1, ret2 := twoRet(1, 2)
	fmt.Println(ret1, ret2)
	ret1, ret2 = twoVarRet(1, 2)
	fmt.Println(ret1, ret2)
	ret1, ret2 = twoVarRet2(1, 2)
	fmt.Println(ret1, ret2)
}
