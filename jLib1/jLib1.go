package jLib1

import "fmt"

// 初始化函数，引入后调用
func init() {
	fmt.Println("jLib1 init")
}
// 大写开头，暴露API
func Hello() {
	fmt.Println("Hello jLib1")
}
// 小写开头，内部使用
func bye() {
	fmt.Println("Bye jLib1")
}