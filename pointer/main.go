package main
import "fmt"


func wrongSwap(a,b int)  {
	temp := a
	a = b
	b = temp
}

func pointerSwap(a,b *int)  {
	temp := *a
	*a = *b
	*b = temp
}


/**
 * &var 获取变量地址
 * *var 根据指针（变量地址）获取变量值
 */
func main()  {
	a,b := 10 , 20
	wrongSwap(a,b)
	fmt.Println(a,b)

	a,b = 50,60
	pointerSwap(&a,&b)
	fmt.Println(a,b)
}