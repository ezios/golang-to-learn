package main

import (
	"fmt"
	"reflect"
)

type Computer struct {
	Cpu string
	Ram int
	Disk int
}

// 反射获取类型和值，输出结构体所有的键值对
func main() {
	param := 1.235
	tp := reflect.TypeOf(param)
	val := reflect.ValueOf(param)

	fmt.Printf("type:%v,value:%v\n", tp, val)

	lenovo := Computer{"i5", 8, 500}
	pc := reflect.ValueOf(lenovo)

	for i := 0; i < pc.NumField(); i++ {
		fmt.Printf("field:%v,value:%v\n", pc.Type().Field(i).Name, pc.Field(i))
	}

}
