package main

import "fmt"

func main() {
	var a []int
	a = append(a, 1)
	fmt.Println("一开始：", a)
	changeSlice(a)
	fmt.Println("修改后：", a)

	a = append(a,33)

	fmt.Println(a)

	for _,num  := range a {
		fmt.Println(num)
	}
}

/**
 * 修改切片
	证明切片传递为引用传递
*/
func changeSlice(a []int) {
	a[0] = 2
}
