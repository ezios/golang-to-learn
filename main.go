package main

import (
	"fmt"
	"learning/jLib1"
)

func main() {
    fmt.Println("Hello, World!")

	m := make(map[string]int)
	
	m["a"] = 1
	m["b"] = 2

	fmt.Println(m)

	value , present := m["a"]

	fmt.Println(value,present)
	value , present = m["c"]

	fmt.Println(value,present)

	jLib1.Hello()
}