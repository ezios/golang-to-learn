package main

import (
	"fmt"
	Person "learning/struct/model"
)

func main() {
	var p1 Person.Person
	Person.SetPerson(&p1,"张三", 18, "男")
	fmt.Println(p1)

	var p2 Person.Person
	Person.SetPerson(&p2,"李四", 19, "女")
	fmt.Println(p2)

	p3 :=Person.Person{}
	p3.SetName("王五")
	fmt.Println(p3)

	p4 := Person.Create()
	p4.SetAge(12)
	p4.SetName("赵六")
	fmt.Println(p4)
}