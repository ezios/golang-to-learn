package model

type Person struct {
	name string
	age  int
	sex  string
}

func SetPerson(this *Person,name string,age int,sex string)  {
	this.name = name
	this.age = age
	this.sex = sex
}

func (p *Person) SetName(name string)  {
	p.name = name
}
func (p *Person) SetAge(age int) {
	p.age = age
}
func (p *Person) SetSex(sex string)  {
	p.sex = sex
}
func (p *Person) GetName() string  {
	return p.name
}
func (p *Person) GetAge() int  {
	return p.age
}
func (p *Person) GetSex() string  {
	return p.sex
}

func Create() Person {
	return Person{}
}